import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('each-page-contacts', 'Integration | Component | each page contacts', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{each-page-contacts}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#each-page-contacts}}
      template block text
    {{/each-page-contacts}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
