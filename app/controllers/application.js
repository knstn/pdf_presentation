/**
 * Created by costa on 16.05.16.
 */

export default Ember.Controller.extend({
  init: function () {
    this._super();
    Ember.run.schedule("afterRender", this, function () {
      this.send("afterRender");
    });
  },
  buildings: Em.computed('model.buildings', function(){
    let buildings = this.get('model.buildings');
    return buildings.filter(function(item, index){

      return !!item.building;
    })
  }),
  buildingsPaged: Em.computed('buildings', function(){
      return this.get('buildings').chunk_inefficient(15);
  }),
  buildingsPaged2: Em.computed('buildings', function(){
      let blocks = [];
      this.get('buildings').forEach(bld=>{
        let localBlocks = bld.blocks.map(el=>{
          el['buildingName'] = bld.building.name;
          return el;
        });
        blocks.pushObjects(localBlocks);
      });

    return blocks.chunk_inefficient(10);

  }),
  actions: {
    afterRender(){
      //$("h1").textfill();
      //$(".object-thumb__name").textfill();
    }
  }
});

