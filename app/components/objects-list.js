import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['objects-list'],
  objects: [
    {
      name: 'Ситидел',
      metro: 'Курская',
      address: 'Пресненская наб. 12',
      classCode: 'A'
    },
    {
      name: 'Ситидел2',
      metro: 'Тверская',
      address: 'Пресненская наб. 12',
      classCode: 'B'
    },
    {
      name: 'Ситидел3',
      metro: 'Арбатская',
      address: 'Пресненская наб. 12',
      classCode: 'C'
    },
    {
      name: 'Ситидел4',
      metro: 'Таганская',
      address: 'Пресненская наб. 12',
      classCode: 'B+'
    },
    {
      name: 'Ситидел5',
      metro: 'Водный стадион',
      address: 'Пресненская наб. 12 очень длинный адрес',
      classCode: 'A++'
    },
    {
      name: 'Ситидел4',
      metro: 'Таганская',
      address: 'Пресненская наб. 12',
      classCode: 'B+'
    },
    {
      name: 'Ситидел5',
      metro: 'Водный стадион',
      address: 'Пресненская наб. 12',
      classCode: 'A++'
    }
  ]
});
