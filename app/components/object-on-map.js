import Ember from 'ember';
import ENV from 'pdf-presentation/config/environment';
export default Ember.Component.extend({
  classNames: ['objects-on-map'],
  imgUrl: Em.computed('buildings', function(){

    let minX = 10000;
    let maxY = 0;

    let minY = 10000;
    let maxX = 0;
    this.get('buildings').forEach(el=>{
      if(el.building.coords.lon < minX){
        minX = el.building.coords.lon;
      }

      if(el.building.coords.lat < minY){
        minY = el.building.coords.lat;
      }

      if(el.building.coords.lon > maxX){
        maxX = el.building.coords.lon;
      }

      if(el.building.coords.lat > maxY){
        maxY = el.building.coords.lat;
      }
    });

    //let url = 'https://static-maps.yandex.ru/1.x/?ll=37.620070,55.753630&z=12&size=450,320&l=map&pt=';
    let url = `https://static-maps.yandex.ru/1.x/?bbox=${minX},${minY}~${maxX},${maxY}&z=12&size=450,320&l=map&pt=`;
    let coords = [];
    this.get('buildings').forEach(el=>{
      coords.push(`${el.building.coords.lon},${el.building.coords.lat},pm2rdm`);
    });
    url += coords.join('~');
    return url;
  }),

  newImgUrl: Em.computed('presID', function(){
    let url = '';
    let presID = this.get('presID');
    if(ENV.environment=='development'){
      url = '';
    }else{
      url = `http://sophie.of.ru/api/presentations/data/get_map_picture?id=${presID}`;
    }
    return url;
  }),

  didRender(){
    let that = this;
    let selector = '#'+this.get('elementId')+' .map-cont';
    let cont = document.querySelector(selector);
    ymaps.ready(init);
    var myMap;

    function init(){
/*
      myMap = new ymaps.Map(cont, {
        center: [37.677751,55.757718],
        zoom: 15
      });*/

     /* [37.677751,55.757718].forEach(el=>{
        let b_coords = [el[0], el[1]];
        let myPlacemark = new ymaps.Placemark(b_coords);
        myMap.geoObjects.add(myPlacemark);
      });


      myMap.behaviors.disable("scrollZoom");
      let w = window.innerWidth;
      if(w < 500){
        myMap.behaviors.disable("drag");
      }

      let collection = myMap.geoObjects;//.getBounds();

      let centerAndZoom = ymaps.util.bounds.getCenterAndZoom(

        collection.getBounds(),

        myMap.container.getSize(),

        myMap.options.get('projection')

      );*/
      //myMap.setZoom(centerAndZoom.zoom-0.2);
      //myMap.setCenter(centerAndZoom.center);
      //that.set('myMap', myMap);
    }
  }
});
