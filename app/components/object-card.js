import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['object-card'],
  blocks: Em.computed('data.blocks', function(){
    let blocks = this.get('data.blocks');
    if(blocks.get('length') > 9){
      blocks = blocks.slice(0, 8);
    }
    return blocks;
  }),
  didRender(){
    let selector = '#' + this.get('elementId') + ' .card-right__map';
    let cont = document.querySelector(selector);
    //let parentWidth =  window.getComputedStyle(document.querySelector('.card-right')).width;

    //this.$('.card-right__map').width(this.$('.card-right').width());
    //cont.style.width = parseInt(parentWidth/2);
    ymaps.ready(init);

    var myMap;
    function init() {

      myMap = new ymaps.Map(cont, {
        center: [37.677751, 55.757718],
        zoom: 15
      });
    }
  }
});
