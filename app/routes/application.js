/**
 * Created by costa on 14.06.16.
 */
import ENV from 'pdf-presentation/config/environment';
export default Ember.Route.extend({
  ENV,
  beforeModel(transition){
    if (!transition.queryParams.id) {
      console.log('NO ID FOR PRESENTATION');
    }
    this.set('presID', transition.queryParams.id);
    this.controllerFor('application').set('presID', transition.queryParams.id);
  },
  model(params){
    let url = '/presentations/data/landing_data?id=' + this.get('presID');
    let env = this.get('ENV');
    if (env.environment === 'development') {
      url = '/data.json';
    }

    return $.getJSON(url).then(function (data) {

      return data;
    }, function (reason) {
      // on rejection
      console.log('Извините, произошла ошибка.');
    });
  },

  afterModel(model){
    //this.controllerFor('application').set('newConfig', model);
    this.controllerFor('application').set('presID', this.get('presID'));
  },
  getJSON(url) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onreadystatechange = handler;
      xhr.responseType = 'json';
      xhr.setRequestHeader('Accept', 'application/json');
      xhr.send();

      function handler() {
        if (this.readyState === this.DONE) {
          if (this.status === 200) {
            resolve(this.response);
          } else {
            reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
          }
        }
      }
    });
  }
});
